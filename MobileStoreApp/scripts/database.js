(function(global){
    app = global.app = global.app || {}
    var data = {
        loginstmt : 'CREATE TABLE  IF NOT EXISTS login(logid text,username,password,email)',
        storestmt:'CREATE TABLE  IF NOT EXISTS store(storeid,brandname,image,authenticate,noofitems)',
        createdb:function(){
            app.db = global.openDatabase('mbstore','3.8.2','Mobile Storage',20*1024*1024,function(err,res){
                data.createtables();
            });
        },
        createtables:function(){
            app.db.transaction(function(tx){
                tx.executeSql(data.loginstmt);
                tx.executeSql(data.storestmt);
            })
        },
        insertlogin:function(model){
            app.db.transaction(function(tx){
                tx.executeSql('INSERT INTO login(logid,username,password,email) VALUES(?,?,?,?)',
            [model.logid,model.username,model.password,model.email]);
            });
    },
        insertstore:function(model){
            console.log(model);
            app.db.transaction(function(tx){
                tx.executeSql('INSERT INTO store(storeid,brandname,image,authenticate,noofitems) VALUES(?,?,?,?,?)',
            [model.storeid,model.brandname,model.image,model.authenticate,model.noofitems]);
            });
    }
    }
    app.data = data;
    global.app = app;

})(window);