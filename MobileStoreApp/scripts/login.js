(function (global) {
    function s4() {
  return Math.floor((1 + Math.random()) * 0x10000)
             .toString(16)
             .substring(1);
 };

function guid() {
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
         s4() + '-' + s4() + s4() + s4();
}
    var LoginViewModel,
        app = global.app = global.app || {};

    LoginViewModel = kendo.data.ObservableObject.extend({
        isLoggedIn: false,
        username: "",
        password: "",
        email:"",

        onLogin: function () {
            var that = this,
                username = that.get("username").trim(),
                password = that.get("password").trim();

            if (username === "" || password === "") {
                navigator.notification.alert("Both fields are required!",
                    function () { }, "Login failed", 'OK');

                return;
            } else {
                app.db.transaction(function(tx){
                    tx.executeSql('select * from login where username= "'+username + '" and password= "'+ password+ '"',[],function(err,res){
                        if(res.rows.length){
                            app.user = res.rows.item(0);                                        
                           
                            localStorage.setItem('user',JSON.stringify(res.rows.item(0)));
                            app.weatherService.viewModel.setname();
                            window.location.href = '#tabstrip-store';
                        }
                        else {
                            navigator.notification.alert("Please check the credentials or SignUp");
                        }
                    })
                });              
            }           
            //that.set("isLoggedIn", true);
        },

        onLogout: function () {
            var that = this;

            that.clearForm();
            that.set("isLoggedIn", false);
        },

        clearForm: function () {
            var that = this;

            that.set("username", "");
            that.set("password", "");
            that.set("email", "");
        },

        checkEnter: function (e) {
            var that = this;

            if (e.keyCode == 13) {
                $(e.target).blur();
                that.onLogin();
            }
        },
        gotoRegister:function(){
            window.location.href = '#tabstrip-register'; 
        },
        onRegister:function(){
            var that = this,
            username = that.get("username").trim(),
            password = that.get("password").trim(),
            email = that.get("email").trim();            
            if (username === "" || password === "" || email === "") {
                navigator.notification.alert("All fields are required!",
                    function () { }, "Login failed", 'OK');

                return;
            } else {
                app.data.insertlogin({logid:guid(),username:username,password:password,email:email});
                 window.location.href = "#tabstrip-login";
                 that.clearForm();
            }
        }
    });
    app.guid = guid;
    app.loginService = {
        viewModel: new LoginViewModel()
    };
})(window);