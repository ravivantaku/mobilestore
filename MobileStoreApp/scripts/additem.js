(function (global) {
    var AddItemViewModel,
        app = global.app = global.app || {}, user = JSON.parse(localStorage.getItem('user'));       
    
    AddItemViewModel = kendo.data.ObservableObject.extend({
        devices: ['Samsung Phone','Samsung Tablet','iPhone','iPhone5','iPad','Nokia Lumia','Micromax Canvas'],
        device:'iPhone',
        brandname:'',
        noofitems:'',        
        onAdditem:function(){
        var that = this,
          brandname = that.get('brandname').trim(),
          device = that.get('device').trim(),
            noofitems = that.get('noofitems')
         console.log({storeid:app.guid(),brandname:brandname,image:device + '.jpg',noofitems:noofitems,authenticate:JSON.parse(localStorage.getItem('user')).logid});
         app.data.insertstore({storeid:app.guid(),brandname:brandname,image:device + '.jpg',noofitems:noofitems,authenticate:JSON.parse(localStorage.getItem('user')).logid});
         that.clearForm();
         window.location.href = '#tabstrip-store';
    },
        clearForm: function () {
            var that = this;

            that.set("device", "iPhone");
            that.set("brandname", "");
            that.set("noofitems", "");
        },
         init: function () {
            var that = this;                       
            kendo.data.ObservableObject.fn.init.apply(that, []);
         }
    });  

    app.AddItemService = {
        viewModel: new AddItemViewModel()
    };
})(window);