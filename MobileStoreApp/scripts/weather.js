(function (global) {
    var WeatherViewModel,
        app = global.app = global.app || {}, user = JSON.parse(localStorage.getItem('user'));       
    function toArray(result) {
    var length = result.rows.length;

    var data = new Array(length);

    for (var i = 0; i < length; i++) {
        data[i] = result.rows.item(i);
    }
    return data;
}
    WeatherViewModel = kendo.data.ObservableObject.extend({
        storeDatasource: null,
        username:'',
        addItem:function(){
            window.location.href = '#tabstrip-additem';
        },
        setname:function(){
           this.set('username',JSON.parse(localStorage.getItem('user')).username);
        },
       init: function () {
            var that = this,
                dataSource;           
            kendo.data.ObservableObject.fn.init.apply(that, []);

            dataSource = new kendo.data.DataSource({
                transport: {
                    read:function(options){
                        app.db.transaction(function(tx) {
				tx.executeSql("select * from store where authenticate = '" +JSON.parse(localStorage.getItem('user')).logid , [], function(tx, result) {
					options.success(toArray(result));    
				});
			});  
                    }
                }
            });

            that.set("storeDatasource", dataSource); 
        }
    });  

    app.weatherService = {
        viewModel: new WeatherViewModel()
    };
})(window);